﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace Jueguito2
{
    class PERSONAJE
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private int fuerza;

        public int Fuerza
        {
            get { return fuerza; }
            set { fuerza = value; }
        }

        private int nivel;

        public int Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        private int experiencia;

        public int Experiencia
        {
            get { return experiencia; }
            set { experiencia = value; }
        }

        private int id_raza;

        public int Id_Raza
        {
            get { return id_raza; }
            set { id_raza = value; }
        }


        private int id_habilidades;

        public int Id_Habilidades
        {
            get { return id_habilidades; }
            set { id_habilidades = value; }
        }

        private RAZA tipo_raza;

        public RAZA Tipo_Raza
        {
            get { return tipo_raza; }
            set { tipo_raza = value; }
        }

        // ------ Hasta aca lo mismo que en color ----- //

        // ------ Explica la clase 10 ------- //

        List<HABILIDAD> habilidades = new List<HABILIDAD>();

        public List<HABILIDAD> Habilidades
        {
            get { return habilidades; }
        }

        public void CrearHabilidad(HABILIDAD habilidad)
        {
            string sql = "insert into tabla_habilidad (id, id_habilidad) values (" + this.id.ToString() + "," + habilidad.Id_Habilidad.ToString() + ")"; //Le pase el id del personaje y habilidad, EL THIS ES PORQUE ESTAMOS DENTRO DE PERSONAJE

            AccesoDatos acceso = new AccesoDatos();

            if (acceso.Escribir(sql)  == 1 ) // Si lo pudo escribir
            {
                this.habilidades.Add(habilidad); // Se agrega la habilidad a la lista porque se pudo guardar, en este caso la responsabilidad es del propio personaje 
            }
        }

        public void ObtenerHabilidad()
        {
            this.habilidades.Clear(); //Limpiamos las habilidades existentes
            string sql = "select* from Vista_Habilidad where id = " + this.id.ToString();

            AccesoDatos acceso = new AccesoDatos();
            DataTable tabla = acceso.Leer(sql); //Esto ya me devuelve una tabla, entonces la tomo y es la que voy a utilizar

            //El cerrar conexion ya ocurrio, ahora lo corremos de manera desconectado ya que se encuentra en memoria
            //Por cada registro de una tabla lo convertimos en una habilidad del jugador

            foreach (DataRow registro in tabla.Rows)
            {
                HABILIDAD h = new HABILIDAD();

                h.Id_Habilidad = int.Parse(registro["id"].ToString());
                h.Tipo_Habilidad = registro["tipo_habilidad"].ToString();

                this.habilidades.Add(h);
            }
        }
    }
}
