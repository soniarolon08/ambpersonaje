﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace Jueguito2
{
    class RAZA
    {
        private int id_raza;

        public int Id_Raza
        {
            get { return id_raza; }
            set { id_raza = value; }
        }

        private string tipo_raza;

        public string Tipo_Raza
        {
            get { return tipo_raza; }
            set { tipo_raza = value; }
        }

        public override string ToString()
        {
            return tipo_raza;
        }


    }
}
