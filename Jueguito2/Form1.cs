﻿using System;
using System.Windows.Forms;

namespace Jueguito2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // -------- NOS TRAE LOS DATOS YA CARGADOS APENAS INICIAMOS EL FORMULARIO ------ //
        private void Form1_Load(object sender, EventArgs e)
        {
            EnlazarRazas();
            EnlazarHabilidades();
            EnlazarPersonajes();
        }

        AccesoDatos acceso = new AccesoDatos();

        // ------ PASAMOS LA LISTA DE RAZAS QUE HICIMOS EN ACCESODATOS ----- //
        void EnlazarRazas()
        {
            comboBox1.DataSource = null; //Descenlazamos todo
            comboBox1.DataSource = acceso.ListarRazas();
        }

        // ------ PASAMOS LA LISTA DE HABILIDADES QUE HICIMOS EN ACCESODATOS ----- //

        void EnlazarHabilidades()
        {
            comboBox2.DataSource = null;
            comboBox2.DataSource = acceso.ListarHabilidad();
        }

        // ------ PASAMOS LA LISTA DE PERSONAJE QUE HICIMOS EN ACCESODATOS ----- //
        void EnlazarPersonajes()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = acceso.ListarPersonajes();
        }


        //-------- BOTON INSERTAR EN GRUPO PERSONAJE -------//

        private void button1_Click(object sender, EventArgs e)
        {

            RAZA raza = (RAZA)comboBox1.SelectedItem;
            string consulta;
            consulta = "Insert into personaje (fuerza, nivel, puntos, experiencia, id_raza) values (" + textBox1.Text + "," + textBox2.Text + "," + textBox3.Text + "," + textBox4.Text + "," + raza.Id_Raza.ToString() + ")";

            if (acceso.Escribir(consulta) == -1)
            {
                MessageBox.Show("Hubo un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            EnlazarPersonajes();
            GC.Collect();
        }

        // -------- BOTON BORRAR EN GRUPO PERSONAJE ------- //
        private void button2_Click(object sender, EventArgs e)
        {
            AccesoDatos acceso = new AccesoDatos();

            foreach (DataGridViewRow registro in dataGridView1.SelectedRows) // Esto va ir iterando todos los elementos seleccionados de la lista
            {

                PERSONAJE informacionnuevapersonaje = (PERSONAJE)registro.DataBoundItem; //Voy a retirar el objeto que esta enlazado

                string sql = "DELETE FROM PERSONAJE WHERE ID='" + informacionnuevapersonaje.Id.ToString() + "'";

                acceso.Escribir(sql);
            }

            EnlazarPersonajes();
            acceso = null;

            GC.Collect();


        }
        // -------- BOTON EDITAR EN GRUPO PERSONAJE ------- //
        private void button3_Click(object sender, EventArgs e)
        {

            RAZA raza = (RAZA)comboBox1.SelectedItem;

            foreach (DataGridViewRow registro in dataGridView1.SelectedRows)
            {
                PERSONAJE personaje = (PERSONAJE)registro.DataBoundItem;

                string sql = "UPDATE PERSONAJE SET";

                sql += " FUERZA=" + textBox1.Text + ", NIVEL=" + textBox2.Text + ", PUNTOS=" + textBox3.Text + ", ESPERIENCIA=" + textBox4.Text + ", ID_RAZA=" + raza.Id_Raza.ToString() + " WHERE ID = " + personaje.Id.ToString();

                AccesoDatos acceso = new AccesoDatos();

                if (acceso.Escribir(sql) == -1)
                {
                    MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Ok");
                }

            }
            EnlazarPersonajes();
        }

        // -------- BOTON ASIGNAR HABILIDADES EN GRUPO LISTADO DE HABILIDADES ------- //

        // -------- Explicacion clase 10 ------- //
        private void button6_Click(object sender, EventArgs e)
        {
            // ¿Hay algun elemento seleccionado en la grilla?

            if (dataGridView1.SelectedRows.Count > 0) 
            {
                PERSONAJE personajito = (PERSONAJE)dataGridView1.SelectedRows[0].DataBoundItem; // El databounditem va a tener el puntero al objeto que esta en memoria

                HABILIDAD habi = (HABILIDAD)comboBox2.SelectedItem;  
                personajito.CrearHabilidad(habi); // Con esto ya se creo la habilidad

                EnlazarHabilidades(personajito); // Enlazamos las habilidades del personaje

            }
        }


         void EnlazarHabilidades(PERSONAJE personajito) // Este metodo nos permite enlazar en una lista las habilidades que tiene el jugador y no en general
        {
            personajito.ObtenerHabilidad(); //Ahi se fuerza a buscar cuales son las habilidades
            listBox1.DataSource = null; // Descenlazamos
            listBox1.DataSource = personajito.Habilidades;
        }


        // Aprete sin querer //
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // ---------- BOTON INSERTAR EN GRUPO DE INSERTAR RAZA ---------- //
        private void button4_Click(object sender, EventArgs e)
        {
            AccesoDatos acceso = new AccesoDatos();

            string sql = "insert into tabla_raza (tipo_raza) values ('" + textBox5.Text + "')";

            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Hubo un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            EnlazarRazas();
            EnlazarPersonajes();
        }

        // ---------- BOTON INSERTAR EN GRUPO DE INSERTAR HABILIDAD ---------- //

        private void button5_Click(object sender, EventArgs e)
        {
            AccesoDatos acceso = new AccesoDatos();

            string sql = "insert into tabla_habilidad (tipo_habilidad) values ('" + textBox5.Text + "')";

            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Hubo un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            EnlazarHabilidades();
            EnlazarPersonajes();

        }

        // ------ ÁPRETE SIN QUERER ----- //
        private void button5_Click_1(object sender, EventArgs e)
        {

        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // ------ ÁPRETE SIN QUERER ----- //
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            PERSONAJE nuevopersonaje = (PERSONAJE)dataGridView1.Rows[e.RowIndex].DataBoundItem;
            textBox1.Text = nuevopersonaje.Fuerza.ToString();
            textBox2.Text = nuevopersonaje.Nivel.ToString();
            textBox3.Text = nuevopersonaje.Puntos.ToString();
            textBox4.Text = nuevopersonaje.Experiencia.ToString();
            comboBox1.SelectedItem = null;

            foreach (object item in comboBox1.Items)
            {
                if (((RAZA)item).Id_Raza == nuevopersonaje.Tipo_Raza.Id_Raza)
                {
                    comboBox1.SelectedItem = item;
                }
            }

            EnlazarHabilidades(nuevopersonaje); // Pasamos las habilidades del personaje
        }
    }
}
